<?php

return [
    'host' => 'dysmsapi.aliyuncs.com',
    'access_key_id' => '',
    'access_key_secret' => '',
    'RegionId' => 'cn-hangzhou',
    'template_code' => "SMS_123132",
    'sign_name' => '',


    'Tools' => [
        'Ali' => \App\Tools\Sms\AliSms::class,
        'Baidu' => \App\Tools\Sms\BaiduSms::class,
        'Jd' => \App\Tools\Sms\JdSms::class
    ]
];
