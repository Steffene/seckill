<?php
declare(strict_types=1);

namespace App\Http\Controllers\Common;

use App\Http\Controllers\Controller;
use App\Tools\Sms\SmsFactory;
use App\Tools\Utils\Number;
use Illuminate\Http\Request;
use App\Tools\Sms\AliSms;

/**
 * Class SmsController
 * @package App\Http\Controllers\Common
 * 发送短信公共控制器
 */
class SmsController extends Controller
{

    public function code(Request  $request) {
        $phoneNumber = $request->get("phoneNumber");

        //这里需要校验手机号码是否正确(可以使用正则表达式或者第三方接口来验证)

        if (!$phoneNumber) {
            return show("001","lease input phoneNumber");
        }

        // 该手机号码需要到redis中查询，该手机号码是否在有效时间内发送多次，如果redis中有该手机号码，就进行拦截

        // 随机生4位验证码
        $code = Number::generateNumber(4);


//        if (!AliSms::sendCode($phoneNumber,(int)$code)) {
//            return show("001","sendFail");
//        }

        if (!SmsFactory::useSms("li",config("Sms.Tools"),[],false)) {
            return show("001","sendFail");
        }

        // 发送成功后验证码需要存入redis,并且有效期为1分钟
        $prefix = "mall_code_pre_";
        $code_expire = 60;

        $key = $prefix.$phoneNumber;

         // set redis
//        cache($key,$code,$code_expire);
        return show("000","sendSuccess");
    }
}
