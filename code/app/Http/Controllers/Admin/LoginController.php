<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\LoginRequest;

class LoginController extends Controller
{

    public function login() {
        return view("admin.login.login");
    }

    public function sign(LoginRequest  $request) {
        $username = $request->get('username');
        $password = $request->get('password');

        if (false) {
            return show(config('code.success'), 'login success');
        }
        return show(config('code.error'),'login fail');

    }
}
