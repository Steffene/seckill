<?php

namespace App\Tools\Utils;

/**
 * Class Number
 * @package App\Tools\Utils
 */
class Number {


    /**
     * @param $length
     * @return string
     */
    public static function generateNumber($length) {
        $array = range(0,9);
        $idxs = array_rand($array,$length);
        shuffle($idxs);
        $code = '';
        foreach ($idxs ?? [] as $idx) {
            $code .= $array[$idx];
        }
        return $code;
    }
}
