<?php
declare(strict_types=1);

namespace App\Tools\Sms;

/**
 * Class SmsFactory
 * @package App\Tools\Sms
 */
class SmsFactory {

//    private $types = [];
//
//    public function __construct()
//    {
//        $this->types = config("Sms.Tools");
//    }

    //如果我们的工厂模式调用的方法是静态的，那么我们就法那会类苦的类对象即可
    //如果不是静态的方法，就需要返回对象(需要实例化)
    public static function useSms($type,$types,$params = [],$instance = true) {
        if (!array_key_exists($type,$types)) {
            return false;
        }

        $className = $types[$type];

        if ($instance == true) {
            $smSclass = (new \ReflectionClass($className))->newInstanceArgs($params);   //建反射类并实例化
        }else {
            $smSclass = $className;
        }
        return $smSclass;
    }


}
