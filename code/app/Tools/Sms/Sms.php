<?php
declare(strict_types=1);

namespace App\Tools\Sms;

interface Sms {

    public static function sendCode(string $phone,int $code):bool;
}
