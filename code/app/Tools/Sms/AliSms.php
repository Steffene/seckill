<?php
declare(strict_types=1);

namespace App\Tools\Sms;

use AlibabaCloud\Client\AlibabaCloud;
use AlibabaCloud\Client\Exception\ClientException;
use Alibabaoud\Client\Exception\ServerException;
use Illuminate\Support\Facades\Log;

class AliSms implements Sms {

    public static function sendCode(string $phone,int $code) :bool {
        AlibabaCloud::accessKeyClient('<accessKeyId>', '<accessSecret>')
            ->regionId('cn-hangzhou')
            ->asDefaultClient();

        try {
            $templateParam = ["code" => $code];
            $result = AlibabaCloud::rpc()
                ->product('Dysmsapi')
                // ->scheme('https') // https | http
                ->version('2017-05-25')
                ->action('AddSmsSign')
                ->method('POST')
                ->host('dysmsapi.aliyuncs.com')
                ->options([
                    'query' => [
                        'RegionId' => "cn-hangzhou",
                        "PhoneNumbers" => $phone,
                        'SignName' => "",
                        "TemplateCode" => "SMS_123132",
                        "TemplateParam" => json_encode($templateParam,JSON_UNESCAPED_UNICODE)
                    ],
                ])
                ->request();

            Log::info("alSms-sendCode-result-{$phone} ".json_encode($result->toArray()));

            if (isset($result['Code']) && $result['Code'] == 'OK') {
                return true;
            }
            return false;
        } catch (ClientException $e) {
            // 这里需要记录日志
            Log::error("alSms-sendCode-error-{$phone} ".$e->getErrorMessage());
            return false;
//            echo $e->getErrorMessage() . PHP_EOL;
        } catch (ServerException $e) {
            // 这里需要记录日志
            Log::error("alSms-sendCode-error-{$phone} ".$e->getErrorMessage());
            return false;
//            echo $e->getErrorMessage() . PHP_EOL;
        }
    }
}
