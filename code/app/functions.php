<?php
// 函数库

if (!function_exists("")) {

    /**
     * @param string $code
     * @param string $message
     * @param array $data
     * @param int $httpStatus
     * 通用json输出
     */
    function show($code = '000',$message = '',$data = [],$httpStatus = 200) {

        $data = [
            'code' => $code,
            'message' => $message,
            'data' => $data,
            'httpStatus' => $httpStatus
        ];
        return json_encode($data,JSON_UNESCAPED_UNICODE);
    }
}
