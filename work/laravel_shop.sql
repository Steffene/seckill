create database if not exists laravel_shop charset=utf8mb4,engine=innodb;

use laravel_shop;

-- 后台管理员表
create table if not exists tp6_admin(
    id mediumint unsigned not null auto_increment,
    username varchar(191) not null comment '用户名',
    password varchar(191) not null comment '密码',
    status tinyint default 0 comment '状态码 1 - 正常 0 - 待审核 99 - 删除',
    last_login_time int comment '最后登录时间',
    last_login_ip varchar(191) comment '最后登录ip',
    operate_user int comment '操作人',
    created_at datetime,
    updated_at datetime,
    primary key(id),
    unique key
    uk_username(username)
)charset=utf8,engine=innodb;


-- 前台用户表
create table if not exists tp6_user(
    id mediumint unsigned not null auto_increment,
    username varchar(191) not null,
    password varchar(191) not null,
    itype tinyiny not null comment 'login mode',
    type tinyint not null,
    sex tinyint not null,
    status tinyint not null,
    operate_user varchar(191) not null,
    created_at datetime,
    updated_at datetime,
    primary key(id),
    unique key uk_username(username)
)chrset=utf8,engine=innodb;


-- 商品分类表
create table if not exists tp6_category(
    id mediumint unsigned not null auto_increment,
    `name` varchar(191) not null comment '分类名',
    pid mediumint unsigned not null comment '上级id,如果是顶级id就为0',
    icon varchar(191) not null comment '分类图标',
    `path` varchar(191) not null,
    status tinyint not null,
    sort mediumint unsigned not null default 0 comment '排序',
    created_at datetime,
    updated_at datetime,
    primary key(id),
    unique key uk_name(name)
)charset=utf8,engine=innodb;


/* 商品模块数据表设计 */
create table if not exists tp6_goods(
    id mediumint unsigned not null auto_increment,
    title varchar(191) not null comment '商品标题',
    category_id mediumint unsigned not null comment '分类id',
    category_id_path varchar(191) not null comment '上级分类的路径,如：1,2,3,便于前台商城用户分类搜索',
    promotion_title varchar(191) not null comment '商品促销语',
    goods_unit varchar(20) not null comment '商品单位',
    keywords varchar(191) not null comment '商品关键字',
    sub_title varchar(191) not null comment '商品副标题',
    stock int not null comment '库存',
    price decimal(10,2) comment '单价',
    cost_price decimal(10,2) comment '原价',
    sku_id char(18) not null comment '商品默认的sku_id',
    is_show_stock tinyint defaul 1 comment '是否显示库存 0 - 不显示 1 - 显示',
    production_at datetime not null comment '商品生产日期',
    goods_specs_type tinyint default 1 comment '商品规格类型 1 - 统一规格(没有规格，就是一个商品)  2 - 多规格（颜色、大小）',
    big_image varchar(191) not null comment '商品大图',
    recommend_image varchar(191) not null comment '商品推荐图',
    carouse_image varchar(191) not null comment '商品详情页轮播图',
    -- `description` varchar(191) not null comment '商品描述,建议将此字段存放到副表',
    is_index_recommend tinyint default 0 comment '是否显示在首页大图商品 0 - 不显示 1 - 显示',
    created_at datetime,
    updated_at datetime,
    primary key (id)
)charset=utf8,engine=innodb;



-- 商品详细表(商品副表)
create table if not exists tp6_goods_detail(
    id mediumint unsigned not null auto_increment,
    goods_id mediumint unsigned not null comment '商品id',
    `description` varchar(191) not null comment '商品描述,建议将此字段存放到副表',
     is_index_recommend tinyint default 0 comment '是否显示在首页大图商品 0 - 不显示 1 - 显示',
    created_at datetime,
    updated_at datetime,
    primary key (id)
)charset=utf8,engine=innodb;

-- 商品规格表
create table if not exists tp6_goods_specs(
    id mediumint unsigned not null auto_increment,
    `name` varchar(191) not null comment '规格名',
    created_at datetime,
    updated_at datetime,
    primary key(id)
)charset=utf8,engine=innodb;


-- 商品规格属性
create table if not exists tp6_goods_value(
    id mediumint unsigned not null auto_increment,
    specs_id mediumint unsigned not null comment '规格id',
    `name` varchar(191) not null comment '规格属性名',
    create_user_id varchar(191) not null comment '建立人id',
    created_at datetime,
    updated_at datetime,
    primary key(id)
)charset=utf8,engine=innodb;


-- 商品-规格中间表
create table if not exists tp6_goods_specs_temp(
    id mediumint unsigned not null auto_increment,
    goods_id mediumint unsigned not null comment '商品id',
    specs_id mediumint unsigned not null comment '规格id',
    created_at datetime,
    updated_at datetime,
    primary key(id)
)charset=utf8,engine=innodb;


-- 商品SKU设计
create table if not exists tp6_goods_sku(
    id mediumint unsigned not null auto_increment,
    goods_id mediumint unsigned not null comment '商品id',
    specs_value_ids varchar(191) not null comment '商品规格属性id集合，如 1,2,3,4',
    price decimal(10,2) comment '单价',
    cost_price decimal(10,2) comment '原价',
    stock int not null comment '库存',
    `status` int not null comment '状态',
    created_at datetime,
    updated_at datetime,
    primary key(id)
)charset=utf8,engine=innodb;
